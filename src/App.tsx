import ConfigProvider from './ConfigProvider'
import ActualApp from './ActualApp'


function App() {
  return (
    <div className="App">
      <ConfigProvider>
        <ActualApp />
      </ConfigProvider>
    </div>
  )
}

export default App
