import { useState, useEffect, useContext, FC, createContext } from 'react'

interface Config {
  apiUrl: string
}

const configContext = createContext<Config>(null as unknown as Config)

const ConfigProvider: FC = ({ children }) => {
  const [config, setConfig] = useState<Config>()

  useEffect(() => {
    (async function() {
      const res = await fetch('/config.json')
      const json = await res.json()
      setConfig(json)
    })()
  }, [])

  return config ? (
    <configContext.Provider value={config}>{children}</configContext.Provider>
  ) :
    <div>
      config not loaded
    </div>
}

export const useConfig = () => useContext(configContext)

export default ConfigProvider
