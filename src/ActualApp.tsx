import { useState, useEffect } from 'react'
import { useConfig } from './ConfigProvider'

interface Post {
  userId: number
  id: number
  title: string
  body: string
}

const ActualApp = () => {
  const config = useConfig()

  const [posts, setPosts] = useState<Post[]>([])

  useEffect(() => {
    (async function() {
      if (config.apiUrl) {
        const res = await fetch(`${config.apiUrl}/posts?_limit=10`)
        const data = await res.json()
        setPosts(data)
      }
    })()
  }, [config.apiUrl])

  return (
    <div>
      <div>
        actual content from: {config.apiUrl}
      </div>
      <ul>
        {posts.map(post => (
          <li key={post.id}>
            {post.title}
          </li>
        ))}
      </ul>
    </div>)
}

export default ActualApp
